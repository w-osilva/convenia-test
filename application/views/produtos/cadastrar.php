<h1>Produtos</h1>
<h4>Cadastrar</h4>

<?php echo form_open('produtos/cadastrar') ?>
<?php echo validation_errors(); ?>

<label>Nome</label>
<input type="text" name="NOME">

<label>Categoria</label>
<select name="ID_CATEGORIA">
    <option value="">--- Selecione ---</option>
    <?php foreach($categorias as $categoria): ?>
        <option value="<?php echo $categoria->ID_CATEGORIA?> <?php echo set_select('ID_CATEGORIA', $categoria->ID_CATEGORIA);?>"><?php echo $categoria->NOME;?></option>
    <?php endforeach; ?>
</select>

<input type="submit" value="Cadastrar">

<?php echo form_close() ?>

