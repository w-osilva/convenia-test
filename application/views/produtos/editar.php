<h1>Produtos</h1>
<h4>Editar</h4>

<?php echo form_open('produtos/editar/'.$produto->ID_PRODUTO) ?>
<?php echo validation_errors(); ?>

<label>Nome</label>
<input type="text" name="NOME" value="<?php echo $produto->NOME ?>">

<label>Categoria</label>
<select name="ID_CATEGORIA">
    <option value="">--- Selecione ---</option>
    <?php foreach($categorias as $categoria): ?>
        <?php if($produto->CATEGORIA == $categoria->NOME):?>
            <option selected="selected" value="<?php echo $categoria->ID_CATEGORIA?> <?php echo set_select('ID_CATEGORIA', $categoria->ID_CATEGORIA);?>"><?php echo $categoria->NOME;?></option>
        <?php else : ?>
            <option value="<?php echo $categoria->ID_CATEGORIA?> <?php echo set_select('ID_CATEGORIA', $categoria->ID_CATEGORIA);?>"><?php echo $categoria->NOME;?></option>
        <?php endif; ?>
    <?php endforeach; ?>
</select>

<input type="submit" value="Cadastrar">

<?php echo form_close() ?>