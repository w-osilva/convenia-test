<h1>Produtos</h1>
<h4>Lista</h4>

<a href="<?php echo base_url('produtos/cadastrar')?>">Cadastrar</a>

<table>
    <tr>
        <th>Id</th>
        <th>Nome</th>
        <th>Categoria</th>
        <th>Data Cadastro</th>
        <th>Ações</th>
    </tr>
    <?php if(isset($produtos)) : if(!empty($produtos)) : foreach($produtos as $key => $produto) : ?>
    <tr>
        <td><?php echo $produto->ID_PRODUTO ?></td>
        <td><?php echo $produto->NOME ?></td>
        <td><?php echo $produto->CATEGORIA ?></td>
        <td><?php echo date('d/m/Y', strtotime($produto->DT_CADASTRO)) ?></td>
        <td>
            <a href="<?php echo base_url('produtos/editar/'.$produto->ID_PRODUTO)?>">Editar</a>
            <a href="<?php echo base_url('produtos/excluir/'.$produto->ID_PRODUTO)?>">Excluir</a>
        </td>
    </tr>
    <?php endforeach; endif; endif; ?>
    <tr></tr>
</table>