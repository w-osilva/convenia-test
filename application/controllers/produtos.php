<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Produtos extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->model('produtos_model');
        $this->load->model('categorias_model');
        $this->form_validation->set_error_delimiters('<p class="alert alert-error"><button class="close" data-dismiss="alert">×</button> ', '</p>');
    }


    public function lista()
	{
        $produtos = $this->produtos_model->getAll();

        $this->template->load('template/base', 'produtos/lista', array(
            'produtos' => $produtos
        ));
	}


    public function cadastrar()
    {
        //define as regras de validação do formulário
        $this->form_validation->set_rules('NOME', 'Nome', 'xss_clean|required');
        $this->form_validation->set_rules('ID_CATEGORIA', 'Categoria', 'xss_clean|required' );

        if($this->form_validation->run()) {
            $dados_produto = array(
                'NOME'=> $this->input->post('NOME'),
                'ID_CATEGORIA' => $this->input->post('ID_CATEGORIA'),
                'DT_CADASTRO' => date('Y-m-d'),
            );

            if($this->produtos_model->insert($dados_produto)) {
                //recupera ultimo id inserido
                $id_produto = $this->db->insert_id();

                //define a mensagem de sucesso
                $this->session->set_flashdata('message', '<p class="alert alert-success"><button class="close" data-dismiss="alert">×</button> Produto cadastrado com sucesso</p>');
                redirect('produtos', 'refresh');
            }
        }

        $categorias = $this->categorias_model->getAll();

        $this->template->load('template/base', 'produtos/cadastrar', array(
            'categorias' => $categorias
        ));
    }


    public function editar($id_produto)
    {
        //define as regras de validação do formulário
        $this->form_validation->set_rules('NOME', 'Nome', 'xss_clean|required');
        $this->form_validation->set_rules('ID_CATEGORIA', 'Categoria', 'xss_clean|required' );

        if($this->form_validation->run()) {
            $dados_produto = array(
                'NOME'=> $this->input->post('NOME'),
                'ID_CATEGORIA' => $this->input->post('ID_CATEGORIA'),
            );

            if($this->produtos_model->update($id_produto, $dados_produto)) {
                //define a mensagem de sucesso
                $this->session->set_flashdata('message', '<p class="alert alert-success"><button class="close" data-dismiss="alert">×</button> Produto editado com sucesso</p>');
                redirect('produtos', 'refresh');
            }
        }

        $categorias = $this->categorias_model->getAll();
        $produto = $this->produtos_model->getById($id_produto);

        $this->template->load('template/base', 'produtos/editar', array(
            'categorias' => $categorias,
            'produto' => $produto
        ));
    }


    public function excluir($id_produto)
    {
        if($this->produtos_model->delete($id_produto)) {
            //define mensagem de sucesso
            $this->session->set_flashdata('message', '<p class="alert alert-success"><button class="close" data-dismiss="alert">×</button>  Produto removido com sucesso</p>');
        }
        redirect('produtos', 'refresh');
    }
}
