<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Categorias_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getAll()
    {
        $this->db->select("c.ID_CATEGORIA, c.NOME");
        $this->db->from('CATEGORIAS c');
        $this->db->order_by('c.NOME ASC');
        return $this->db->get()->result();
    }
}
