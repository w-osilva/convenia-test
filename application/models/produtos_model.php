<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Produtos_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();

    }

    public function getAll()
    {
        $this->db->select("p.ID_PRODUTO, p.NOME, c.NOME as CATEGORIA, p.DT_CADASTRO");
        $this->db->from('PRODUTOS p');
        $this->db->join('CATEGORIAS c', 'p.ID_CATEGORIA = c.ID_CATEGORIA');
        $this->db->order_by('p.DT_CADASTRO DESC');
        return $this->db->get()->result();
    }


    public function getById($id_produto)
    {
        $this->db->select("p.ID_PRODUTO, p.NOME, c.NOME as CATEGORIA, p.DT_CADASTRO");
        $this->db->from('PRODUTOS p');
        $this->db->join('CATEGORIAS c', 'p.ID_CATEGORIA = c.ID_CATEGORIA');
        $this->db->where("p.ID_PRODUTO", $id_produto);
        return $this->db->get()->row();
    }


    public function insert($produto)
    {
        return $this->db->insert('PRODUTOS', $produto);
    }


    public function update($id_produto, $produto){
        $this->db->where('ID_PRODUTO', $id_produto);
        return $this->db->update('PRODUTOS', $produto);
    }


    function delete($id_produto){
        $this->db->where('ID_PRODUTO', $id_produto);
        return $this->db->delete('PRODUTOS');
    }
}
